# Tracer

`Tracer` is an Elixir library to print traces of function calls for debugging purposes.

## Motivation

Sometimes you want to trace calls to some functions. For instance, you have a module:

```
defmodule M do
  ...

  def f(x) do
    # f function body
    ...
  end
  ...
end
```

and a client code:

```
defmodule C do
  ...
    M.f(z) # first call
  ...
    M.f(z) # second call
  ...
end
```

And you want to trace calls to function `M.f/1` in all the client code `C`.

If you have access to code in `M` you can instrument the module in this way:
```
defmodule M do
  require Logger
  ...

  def f(x) do
    result = f(x)
    Logger.debug("CALL f(#{inspect x}) RETURNS #{inspect result}")
    result
  end

  defp f_(x) do
    # f function body
    ...
  end
  ...
end
```

This change makes every call to `M.f/1` from any other module to run the instrumented code.

If you want to avoid this you will need to instrument the client code, adding a function and modifying every call to `M.f/1`:
```
defmodule C do
  defp M_f(x) do
    result = M.f(x)
    Logger.debug("CALL f(#{inspect x}) RETURNS #{inspect result}")
    result
  end

  ...
    M_f(z) # first call
  ...
    M_f(z) # second call
  ...
end
```

`Tracer` allows tracing a given function by introducing a surgical
intervention in the client code:

```
defmodule C do
  use Tracer, module: M, trace: [f: 1], level: :debug

  ...
    M.f(z) # first call
  ...
    M.f(z) # second call
  ...
end
```

After this, function call `M.f(z)` will run an instrumented code that
traces the call to the actual function `f`.

## Use

```
  use Tracer,
    module: module_name, # honors aliases and imports (default: current module)
    trace: functions, # keyword with function name as key and arity as value
    level: debug_level # :info, :debug, etc.
```

## Examples

Examples can be found at `test/tracer_test.exs`.

## Limitations

At this moment the library has some limitations:

- Unexpected behaviour with macros
- Unexpected behaviour with functions in `require` and `use`

## Ideas and TODO List

- IMPORTANT QUESTION: which tech to use? elixir macros as we are doing
  now? (it has its limitations). Erlang "dynamic" tracing
  (`erlang:trace`)? (can we follow the call context?)
- IDEA: although originally tracer was thought as a "tracer" :) we
  would like to explore tracing properties. This means that tracer
  might intercept entry and exit points in order to "verify"
  preconditions and postconditions.
- IDEA: instead of or additionally to Logger, other functions could be
  used to "trace", for instance, a new attribute `:log` (or `:with`)
  might accept a function `f` with type `String.t() -> any()`
- What should we do if the programmer tries to trace a macro?
- Other thing to think about is the order of macro expansions when the
  library interacts with other macro libraries like Makina or Ecto.
- Check that file and line in the log is exactly the call point
- Check that functions to be "traced" exists and are accessible (public?)
- Trace format:
  - One line with all information (call, and return and timestamps and
    execution time)
  - One line for the call and one line for the result
  - Nested level (is this possible consulting the stack?)
  - Human-readable vs syslog standard
- Can times get aggregated, so the library might be used to benchmark
  code?
  - GenServer to store aggregated execution times per function
  - but also per actual parameters (abstracted)!
- Detect lasting calls: introduce attribute `:timeout`
- Allow `level: none` to avoid instrumentation.
- Once a data is aggregated some properties on traces might be
  captured, e.g. STARTS without ENDS (obvious)
- There might be a strong relationship with benchmarking and profiling
  libraries: check Erlang or Elixir libraries like `recon`, `redbug`,
  `rexbug`. `opentelemetry`, `eprof`, `fprof` or `cprof`.
- Have a look at `erlang:trace` and `erlang:trace_pattern`.
- What happen when the programmer uses apply?
- We have had an interesting discussion about the context of the
  execution of the functions to be traced.
- Extract and check every call honor type specs (clear with specs, can
  we do something with communication? session types?).
