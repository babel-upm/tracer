defmodule TracerTest do
  use ExUnit.Case
  import ExUnit.CaptureLog

  doctest Tracer

  defmodule Example1 do
    def f, do: :ok
    def f(_x), do: :ok
    def f(_x, _y), do: :ok
  end

  defmodule Example2 do
    def g, do: :ok
    def g(_x), do: :ok
    def g(_x, _y), do: :ok
  end

  ######################################################################
  ## Qualified
  defmodule Q.Test1 do
    use Tracer, module: Example1, trace: [f: 0, f: 1, f: 2]

    def main do
      Example1.f()
      Example1.f(1)
      Example1.f(1, 1 + 2)
    end
  end

  test "client trace with qualified calls" do
    test = fn -> assert Q.Test1.main() == :ok end
    trace = capture_log(test)

    IO.puts(trace)

    assert trace =~ "STARTS TracerTest.Example1.f()"
    assert trace =~ "ENDS TracerTest.Example1.f() ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(1)"
    assert trace =~ "ENDS TracerTest.Example1.f(1) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(1, 3)"
    assert trace =~ "ENDS TracerTest.Example1.f(1, 3) ==> :ok"
  end

  defmodule Q.Test2 do
    use Tracer, module: Example1, trace: [f: 0, f: 1, f: 2]
    use Tracer, module: Example2, trace: [g: 0, g: 1, g: 2]

    def main do
      Example1.f()
      Example1.f(1)
      Example1.f(1, 1 + 2)
      Example2.g()
      Example2.g(1)
      Example2.g(1, 1 + 2)
    end
  end

  test "client trace with qualified calls from 2 different modules" do
    test = fn -> assert Q.Test2.main() == :ok end
    trace = capture_log(test)

    IO.puts(trace)

    assert trace =~ "STARTS TracerTest.Example1.f()"
    assert trace =~ "ENDS TracerTest.Example1.f() ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(1)"
    assert trace =~ "ENDS TracerTest.Example1.f(1) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(1, 3)"
    assert trace =~ "ENDS TracerTest.Example1.f(1, 3) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g()"
    assert trace =~ "ENDS TracerTest.Example2.g() ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g(1)"
    assert trace =~ "ENDS TracerTest.Example2.g(1) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g(1, 3)"
    assert trace =~ "ENDS TracerTest.Example2.g(1, 3) ==> :ok"
  end

  defmodule Q.Test3 do
    use Tracer, module: Example1, trace: [f: 0, f: 1, f: 2]
    use Tracer, module: Example2, trace: [g: 0, g: 1, g: 2]

    def main do
      Example1.f()
      |> Example1.f()
      |> Example1.f(1 + 2)

      Example2.g()
      |> Example2.g()
      |> Example2.g(1 + 2)
    end
  end

  test "client trace with qualified calls from 2 different modules with pipes" do
    test = fn -> assert Q.Test3.main() == :ok end
    trace = capture_log(test)

    IO.puts(trace)

    assert trace =~ "STARTS TracerTest.Example1.f()"
    assert trace =~ "ENDS TracerTest.Example1.f() ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(:ok)"
    assert trace =~ "ENDS TracerTest.Example1.f(:ok) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(:ok, 3)"
    assert trace =~ "ENDS TracerTest.Example1.f(:ok, 3) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g()"
    assert trace =~ "ENDS TracerTest.Example2.g() ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g(:ok)"
    assert trace =~ "ENDS TracerTest.Example2.g(:ok) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g(:ok, 3)"
    assert trace =~ "ENDS TracerTest.Example2.g(:ok, 3) ==> :ok"
  end

  ######################################################################
  # Alias
  defmodule A.Test1 do
    alias Example1, as: Aliased
    use Tracer, module: Aliased, trace: [f: 0, f: 1, f: 2]

    def main do
      Aliased.f()
      Aliased.f(1)
      Aliased.f(1, 1 + 2)
    end
  end

  test "client with alias" do
    test = fn -> assert A.Test1.main() == :ok end
    trace = capture_log(test)

    IO.puts(trace)

    assert trace =~ "STARTS TracerTest.Example1.f()"
    assert trace =~ "ENDS TracerTest.Example1.f() ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(1)"
    assert trace =~ "ENDS TracerTest.Example1.f(1) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(1, 3)"
    assert trace =~ "ENDS TracerTest.Example1.f(1, 3) ==> :ok"
  end

  defmodule A.Test2 do
    alias Example1, as: Aliased1
    alias Example2, as: Aliased2
    use Tracer, module: Aliased1, trace: [f: 0, f: 1, f: 2]
    use Tracer, module: Aliased2, trace: [g: 0, g: 1, g: 2]

    def main do
      Aliased1.f()
      Aliased1.f(1)
      Aliased1.f(1, 1 + 2)
      Aliased2.g()
      Aliased2.g(1)
      Aliased2.g(1, 1 + 2)
    end
  end

  test "client trace with alias from 2 different modules" do
    test = fn -> assert A.Test2.main() == :ok end
    trace = capture_log(test)

    IO.puts(trace)

    assert trace =~ "STARTS TracerTest.Example1.f()"
    assert trace =~ "ENDS TracerTest.Example1.f() ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(1)"
    assert trace =~ "ENDS TracerTest.Example1.f(1) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(1, 3)"
    assert trace =~ "ENDS TracerTest.Example1.f(1, 3) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g()"
    assert trace =~ "ENDS TracerTest.Example2.g() ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g(1)"
    assert trace =~ "ENDS TracerTest.Example2.g(1) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g(1, 3)"
    assert trace =~ "ENDS TracerTest.Example2.g(1, 3) ==> :ok"
  end

  defmodule A.Test3 do
    alias Example1, as: Aliased1
    alias Example2, as: Aliased2
    use Tracer, module: Aliased1, trace: [f: 0, f: 1, f: 2]
    use Tracer, module: Aliased2, trace: [g: 0, g: 1, g: 2]

    def main do
      Aliased1.f()
      |> Aliased1.f()
      |> Aliased1.f(1 + 2)

      Aliased2.g()
      |> Aliased2.g()
      |> Aliased2.g(1 + 2)
    end
  end

  test "client trace with alias from 2 different modules with pipes" do
    test = fn -> assert A.Test3.main() == :ok end
    trace = capture_log(test)

    IO.puts(trace)

    assert trace =~ "STARTS TracerTest.Example1.f()"
    assert trace =~ "ENDS TracerTest.Example1.f() ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(:ok)"
    assert trace =~ "ENDS TracerTest.Example1.f(:ok) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(:ok, 3)"
    assert trace =~ "ENDS TracerTest.Example1.f(:ok, 3) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g()"
    assert trace =~ "ENDS TracerTest.Example2.g() ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g(:ok)"
    assert trace =~ "ENDS TracerTest.Example2.g(:ok) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g(:ok, 3)"
    assert trace =~ "ENDS TracerTest.Example2.g(:ok, 3) ==> :ok"
  end

  ######################################################################
  # Import
  defmodule I.Test1 do
    import Example1
    use Tracer, module: Example1, trace: [f: 0, f: 1, f: 2]

    def main do
      f()
      f(1)
      f(1, 1 + 2)
    end
  end

  test "client trace with import" do
    test = fn -> assert I.Test1.main() == :ok end
    trace = capture_log(test)

    IO.puts(trace)

    assert trace =~ "STARTS TracerTest.Example1.f()"
    assert trace =~ "ENDS TracerTest.Example1.f() ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(1)"
    assert trace =~ "ENDS TracerTest.Example1.f(1) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(1, 3)"
    assert trace =~ "ENDS TracerTest.Example1.f(1, 3) ==> :ok"
  end

  defmodule I.Test2 do
    import Example1
    import Example2
    use Tracer, module: Example1, trace: [f: 0, f: 1, f: 2]
    use Tracer, module: Example2, trace: [g: 0, g: 1, g: 2]

    def main do
      f()
      f(1)
      f(1, 1 + 2)
      g()
      g(1)
      g(1, 1 + 2)
    end
  end

  test "client trace with imported functions from 2 different modules" do
    test = fn -> assert I.Test2.main() == :ok end
    trace = capture_log(test)

    IO.puts(trace)

    assert trace =~ "STARTS TracerTest.Example1.f()"
    assert trace =~ "ENDS TracerTest.Example1.f() ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(1)"
    assert trace =~ "ENDS TracerTest.Example1.f(1) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(1, 3)"
    assert trace =~ "ENDS TracerTest.Example1.f(1, 3) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g()"
    assert trace =~ "ENDS TracerTest.Example2.g() ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g(1)"
    assert trace =~ "ENDS TracerTest.Example2.g(1) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g(1, 3)"
    assert trace =~ "ENDS TracerTest.Example2.g(1, 3) ==> :ok"
  end

  defmodule I.Test3 do
    import Example1
    import Example2
    use Tracer, module: Example1, trace: [f: 0, f: 1, f: 2]
    use Tracer, module: Example2, trace: [g: 0, g: 1, g: 2]

    def main do
      f()
      |> f()
      |> f(1 + 2)

      g()
      |> g()
      |> g(1 + 2)
    end
  end

  test "client trace with imported functions from 2 different modules with pipes" do
    test = fn -> assert I.Test3.main() == :ok end
    trace = capture_log(test)

    IO.puts(trace)

    assert trace =~ "STARTS TracerTest.Example1.f()"
    assert trace =~ "ENDS TracerTest.Example1.f() ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(:ok)"
    assert trace =~ "ENDS TracerTest.Example1.f(:ok) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example1.f(:ok, 3)"
    assert trace =~ "ENDS TracerTest.Example1.f(:ok, 3) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g()"
    assert trace =~ "ENDS TracerTest.Example2.g() ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g(:ok)"
    assert trace =~ "ENDS TracerTest.Example2.g(:ok) ==> :ok"
    assert trace =~ "STARTS TracerTest.Example2.g(:ok, 3)"
    assert trace =~ "ENDS TracerTest.Example2.g(:ok, 3) ==> :ok"
  end

  ######################################################################
  # Module defined functions
  defmodule M.Test1 do
    use Tracer, trace: [f: 0, f: 1, f: 2]
    def f, do: :ok
    def f(_x), do: :ok
    def f(_x, _y), do: :ok

    def main do
      f()
      f(1)
      f(1, 1 + 2)
    end
  end

  test "client trace its own functions" do
    test = fn -> assert M.Test1.main() == :ok end
    trace = capture_log(test)

    IO.puts(trace)

    assert trace =~ "STARTS TracerTest.M.Test1.f()"
    assert trace =~ "ENDS TracerTest.M.Test1.f() ==> :ok"
    assert trace =~ "STARTS TracerTest.M.Test1.f(1)"
    assert trace =~ "ENDS TracerTest.M.Test1.f(1) ==> :ok"
    assert trace =~ "STARTS TracerTest.M.Test1.f(1, 3)"
    assert trace =~ "ENDS TracerTest.M.Test1.f(1, 3) ==> :ok"
  end

  defmodule M.Test2 do
    use Tracer, trace: [f: 0, f: 1, f: 2]
    def f, do: :ok
    def f(_x), do: :ok
    def f(_x, _y), do: :ok

    def main do
      f()
      |> f()
      |> f(1 + 2)
    end
  end

  test "client trace its own functions with pipes" do
    test = fn -> assert M.Test2.main() == :ok end
    trace = capture_log(test)

    IO.puts(trace)

    assert trace =~ "STARTS TracerTest.M.Test2.f()"
    assert trace =~ "ENDS TracerTest.M.Test2.f() ==> :ok"
    assert trace =~ "STARTS TracerTest.M.Test2.f(:ok)"
    assert trace =~ "ENDS TracerTest.M.Test2.f(:ok) ==> :ok"
    assert trace =~ "STARTS TracerTest.M.Test2.f(:ok, 3)"
    assert trace =~ "ENDS TracerTest.M.Test2.f(:ok, 3) ==> :ok"
  end

  test "client traces a globally traced function" do
    test = fn -> assert M.Test2.f() == :ok end
    trace = capture_log(test)

    IO.puts(trace)

    assert trace =~ "STARTS TracerTest.M.Test2.f()"
    assert trace =~ "ENDS TracerTest.M.Test2.f() ==> :ok"
  end
end
