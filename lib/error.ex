defmodule Tracer.Error do
  defexception message: "tracer error"

  @spec throw_error(String.t(), Macro.Env.t()) :: :none
  def throw_error(message, env) do
    error = %Tracer.Error{message: message}
    stacktrace = Macro.Env.stacktrace(env)
    reraise error, stacktrace
  end
end
