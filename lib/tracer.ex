defmodule Tracer do
  alias Tracer.Error

  @options [:module, :level, :trace]

  @spec __using__(Keyword.t()) :: Macro.t()
  defmacro __using__(options) do
    Module.register_attribute(__CALLER__.module, :module, accumulate: true)
    Module.register_attribute(__CALLER__.module, :trace, accumulate: true)
    Module.register_attribute(__CALLER__.module, :level, accumulate: true)

    configure_module(options, __CALLER__)

    quote do
      import Kernel, except: [def: 2, def: 1]
      import unquote(__MODULE__)
      require unquote(__MODULE__)
      require Logger
      @before_compile unquote(__MODULE__)
    end
  end

  @spec configure_module(Keyword.t(), Macro.Env.t()) :: :ok
  defp configure_module(options, env) do
    if not Keyword.keyword?(options) do
      Error.throw_error("options should be passed in a keyword list.", env)
    end

    for option <- Keyword.keys(options),
        option not in @options do
      Error.throw_error("option '#{option} not supported", env)
    end

    module =
      if(options[:module], do: options[:module], else: env.module)
      |> Macro.expand(env)

    Code.ensure_loaded(module)

    if module in Module.get_attribute(env.module, :module) do
      Error.throw_error("Tracer already defined on module '#{inspect(module)}'", env)
    end

    Module.put_attribute(env.module, :module, module)

    if options[:trace] do
      for {f, a} = function <- options[:trace],
          module != env.module and Code.ensure_loaded?(module),
          not function_exported?(module, f, a) do
        "Function '#{inspect([function])}' not defined in module '#{inspect(module)}'"
        |> Error.throw_error(env)
      end

      Module.put_attribute(env.module, :trace, {module, options[:trace]})
    end

    level = if options[:level], do: options[:level], else: :debug
    Module.put_attribute(env.module, :level, {module, level})
  end

  @spec def(Macro.t(), Macro.t() | nil) :: Macro.t()
  defmacro def(call, expr \\ nil) do
    modules = Module.get_attribute(__CALLER__.module, :module)
    levels = Module.get_attribute(__CALLER__.module, :level)
    traces = Module.get_attribute(__CALLER__.module, :trace)

    expr = Macro.postwalk(expr, &explicit_args_in_pipes/1)

    expr =
      for module <- modules, reduce: expr do
        acc ->
          level = Keyword.get(levels, module)
          trace = Keyword.get(traces, module)
          Macro.postwalk(acc, &log_call(&1, module, trace, level, __CALLER__))
      end

    quote do
      Kernel.def(unquote(call), unquote(expr))
    end
  end

  @spec __before_compile__(Macro.Env.t()) :: Macro.t()
  defmacro __before_compile__(_env) do
    module = __CALLER__.module
    levels = Module.get_attribute(module, :level)
    traces = Module.get_attribute(module, :trace) |> Keyword.get(module, [])

    for {name, arity} = f <- Module.definitions_in(module, :def), f in traces do
      level = Keyword.get(levels, module)
      {_, _, location, _} = Module.get_definition(module, f)
      line = Keyword.get(location, :line, 1)
      args = Macro.generate_arguments(arity, module)
      call = tc_call(:super, args)
      log = generate_log_code(module, name, args, level, call)
      Module.make_overridable(module, [f])

      quote context: module, line: line do
        def unquote(name)(unquote_splicing(args)) do
          unquote(log)
        end
      end
    end
  end

  @spec log_call(
          ast :: Macro.t(),
          module :: Macro.t(),
          calls :: Keyword.t(),
          level :: Logger.level(),
          caller_env :: Macro.Env.t()
        ) ::
          Macro.t()
  # case Module.function( args )
  defp log_call(ast = {{:., _env, [m, f]}, _env1, args}, module, calls, level, env) do
    m = Macro.expand(m, env)

    if m == module and {f, length(args)} in calls do
      call = tc_call(m, f, args)
      generate_log_code(m, f, args, level, call)
    else
      ast
    end
  end

  # case function( args ) where function is defined in the module or imported
  defp log_call(ast = {f, _env, args}, module, calls, level, env) when is_list(args) do
    modules = Macro.Env.lookup_import(env, {f, length(args)}) |> Enum.map(&elem(&1, 1))

    cond do
      module in modules and {f, length(args)} in calls ->
        call = tc_call(f, args)
        generate_log_code(module, f, args, level, call)

      {f, length(args)} in calls ->
        call = tc_call(f, args)
        generate_log_code(module, f, args, level, call)

      true ->
        ast
    end
  end

  # otherwise
  defp log_call(ast, _, _, _, _), do: ast

  @spec explicit_args_in_pipes(Macro.t()) :: Macro.t()
  defp explicit_args_in_pipes({:|>, env, [arg1, arg2]}) do
    piped_to_then(arg2) |> then(fn arg2 -> {:|>, env, [arg1, arg2]} end)
  end

  defp explicit_args_in_pipes(ast), do: ast

  @spec generate_log_code(Macro.t(), atom(), Macro.t(), Logger.level(), Macro.t()) :: Macro.t()
  defp generate_log_code(m, f, args, level, call) do
    quote do
      args = unquote(args) |> Enum.map(&inspect/1) |> Enum.join(", ")
      pid = self() |> :erlang.pid_to_list() |> to_string()
      {:current_stacktrace, [_entry1, entry2 | _]} = Process.info(self(), :current_stacktrace)
      {_module, _function, _arity, [file: source, line: line]} = entry2
      call = "#{inspect(unquote(m))}.#{unquote(f)}(#{args})"
      start_msg = "Tracer: #{pid} STARTS #{call} [#{source}:#{line}]"

      Logger.log(unquote(level), start_msg)

      unquote(call)

      return_msg = "Tracer: #{pid} ENDS #{call} ==> #{inspect(result)} [#{time}us]"

      Logger.log(unquote(level), return_msg)

      result
    end
  end

  @spec tc_call(Macro.t(), atom(), Macro.t()) :: Macro.t()
  defp tc_call(m, f, args) do
    quote do
      {time, result} = :timer.tc(unquote(m), unquote(f), unquote(args))
    end
  end

  defp tc_call(f, args) do
    quote do
      {time, result} = :timer.tc(fn -> unquote(f)(unquote_splicing(args)) end)
    end
  end

  defp piped_to_then(ast = {:then, _, _}), do: ast

  defp piped_to_then({f, _env, nil}) do
    x = Macro.unique_var(:x, nil)

    quote do
      then(fn unquote(x) -> unquote(f)(unquote_splicing([x])) end)
    end
  end

  defp piped_to_then({f, _env, args}) do
    x = Macro.unique_var(:x, nil)

    quote do
      then(fn unquote(x) -> unquote(f)(unquote_splicing([x] ++ args)) end)
    end
  end
end
